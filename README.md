WideResnet Architecture for images of size 64 trained on the IMDB-WIKI dataset

0) sudo apt install git python-pip

1) git clone

2) cd dfrws_demo

3) sudo apt-get install build-essential cmake libgtk-3-dev libboost-all-dev

4) pip install -r requirements.txt

5) python demo.py

It takes around 15 minutes to download the weights file

References: https://github.com/yu4u/age-gender-estimation